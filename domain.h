#ifndef DOMAIN_H
#define DOMAIN_H

#include "Types.h"

#include <cmath>
#include <cstdlib>

namespace SIWIR2 {


    class domain{

    private:

        real_d xcorner_ ;
        real_d ycorner_ ;
        real_d xlength_ ;
        real_d ylength_ ;

    public:

        // constructor
        explicit domain(const real_d _xcorner,
                        const real_d _ycorner,
                        const real_d _xlength,
                        const real_d _ylength );

        ~domain() ;

        const real_d x() const ;
        const real_d y() const ;
        const real_d xlength() const ;
        const real_d ylength() const ;

    };

}


#endif // DOMAIN_H
