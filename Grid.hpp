//
//  Grid.hpp
//  Assignment_1
//
//  Created by Sagar Dolas on 25/04/16.
//  Copyright © 2016 Sagar Dolas. All rights reserved.
//

#ifndef Grid_hpp
#define Grid_hpp

#include <stdio.h>
#include <iostream>
#include <vector>
#include <cmath>
#include "Types.h"
#include <omp.h>

namespace SIWIR2 {
    
    const static real_d pie = 4.0 * atan(1.0) ;
    namespace MULTIGRID {
        
        enum GridType {
            Ugrid ,
            Fgrid ,
            Rgrid
        };
        
        class Grid {

        private:
            
            const real_l level_ ;
            const SIWIR2::MULTIGRID::GridType gridType_ ;
            std::vector<real_d> data;
            real_l numGridPointsT_ ;
            real_l numGridpointsX_ ;

            
        public:
            // constructor
            explicit Grid(const real_l _level, const MULTIGRID::GridType _types) ;
            ~Grid() ;
            
            // Access Operator
            const real_d& operator()(const real_l x_dir,const real_l y_dir ) const ;
            real_d& operator() (const real_l x_dir,const real_l y_dir) ;
            
            // Initialising the grids , allocating the memory
            void allocate() ;
            
            // Reset and Resize functions s
            void reset() ; // Clears and reset to zero
            void reset(const real_l size_) ;
            void resize(real_d num = 0.0) ;
            void resize(const real_l size_ ,const real_d num=0.0 ) ;
            
            // Grid Type
            const SIWIR2::MULTIGRID::GridType get_Grid_Type() const ;
            
            //
            const real_l get_Total_Num_Grids() const ;
            const real_l get_Num_Grids() const ;
            const real_l get_Size() const ;
            
            // Cordinates
            real_l getNx() const ;
            real_l getNy() const ;

            //Returning Iterator
            std::vector<real_d>::const_iterator getbegin() const ;
            std::vector<real_d>::const_iterator getlast() const ;
        };
    }
}

#endif /* Grid_hpp */
