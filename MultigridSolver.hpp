//
//  MultigridSolver.hpp
//  Assignment_1
//
//  Created by Sagar Dolas on 25/04/16.
//  Copyright © 2016 Sagar Dolas. All rights reserved.
//

#ifndef MultigridSolver_hpp
#define MultigridSolver_hpp

#include <stdio.h>
#include <iostream>
#include "VcycleFormation.hpp"
#include "Grid.hpp"
#include "Stencil.hpp"
#include "Types.h"
#include <omp.h>
#include <vector>
#include <numeric>
#include <math.h>
#include "Time.hpp"
#include <cmath>

namespace SIWIR2 {
    namespace MULTIGRID {
        
        enum ninePointUpdate {
            Restriction ,
            Interpolation
        };
        
        class Solver : public SIWIR2::MULTIGRID::VcycleFormation {
        private:
            
            real_l numVcycle_ ;
            real_l numPreSmoothners_ ;
            real_l numPostSmoothners_ ;
            std::vector<real_d> error_ ;

            
        public:

            std::vector<real_d> residualarray_;
            Solver(const real_d _length,
                   const real_d _xcorner,
                   const real_d _ycorner,
                   const size_t _numTotalLevel,
                   const real_l _numOfPresmths,
                   const real_l _numOfPostsmths,
                   const real_l _numVcycle,
                   const SIWIR2::MULTIGRID::BOUNDARY _boundaryType ) ;
            ~Solver() ;
        
            // SOLVER FUNCTIONS
            void start_Solver() ;
            void rb_Gauss_Siedel_Smoother(const real_l _level,const SIWIR2::MULTIGRID::GridType _type) ;
            void full_Weighted_Restriction(const real_l _finerLevel, const real_l _coarserLevel) ;
            void Bi_Lin_Interpolation(const real_l _coarserLevel, const real_l _finerLevel) ;
            void calculate_Residual(const real_l _level) ;
            const real_d five_Point_Update(const real_l pos,const real_l x, const real_l y ) ;
            const real_d nine_Point_Update(const real_l level ,const real_l x ,const real_l y, const SIWIR2::MULTIGRID::ninePointUpdate _ninePointUpdate) ;
            void update_F(const real_l _level) ;
            
            // SIMULATION FUNCTIONS 
            void start_Simulation() ;
            void calculate_Residual_Norm() ;
            const real_d compute_Error() ;
            const real_d convergence_Rate(real_l index_) ;
            
            // Reset Value of F after each Vcycle
            void reset_F() ;
            void reset_U() ; 
        };
        
    }
}


#endif /* MultigridSolver_hpp */
