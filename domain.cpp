

#include "domain.h"

SIWIR2::domain::domain(const real_d _xcorner,
                       const real_d _ycorner,
                       const real_d _xlength,
                       const real_d _ylength) : xcorner_(_xcorner),ycorner_(_ycorner),xlength_(_xlength),ylength_(_ylength){


}

SIWIR2::domain::~domain(){

}

const real_d SIWIR2::domain::x() const{

    return xcorner_ ;
}

const real_d SIWIR2::domain::y() const {\

    return ycorner_ ;
}

const real_d SIWIR2::domain::xlength() const {

    return std::fabs(xlength_) ;
}

const real_d SIWIR2::domain::ylength() const {

    return std::fabs(ylength_) ;
}


